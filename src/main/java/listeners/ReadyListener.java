package listeners;

import com.panamahitek.ArduinoException;
import com.panamahitek.PanamaHitek_Arduino;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ReadyListener implements SerialPortEventListener {
    static int id=0;
    private PanamaHitek_Arduino panamaHitekArduino;

    public ReadyListener(PanamaHitek_Arduino panamaHitek_arduino) {
        this.panamaHitekArduino = panamaHitek_arduino;
    }

    @Override
    public void serialEvent(SerialPortEvent serialPortEvent) {

            try {
                if (this.panamaHitekArduino.isMessageAvailable()) {
                    //Se imprime el mensaje recibido en la consola
                    String msg =panamaHitekArduino.printMessage();
                    System.out.println(msg);

                    if(msg.contains("Estoy listo")) {
                        panamaHitekArduino.sendData(Integer.toString(90 * id) + '\n');

                        panamaHitekArduino.flushSerialPort();
                        id++;
                        if (id == 3) id = 0;//0-90-180-ciclico.
                    }else if(msg.contains("current angle")  ){
                        System.out.println(msg);
                    }
                }
            } catch (SerialPortException | ArduinoException ex) {
                Logger.getLogger(PanamaHitek_Arduino.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
}
