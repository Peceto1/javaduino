
import com.panamahitek.ArduinoException;
import com.panamahitek.PanamaHitek_Arduino;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import listeners.ReadyListener;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ArduinoConnection {

    private PanamaHitek_Arduino panamaHitekArduino;
    private  SerialPortEventListener listener;
    private int dataRate;
    private String portName;

    public ArduinoConnection(String portName,int dataRate) {
      panamaHitekArduino   = new PanamaHitek_Arduino();
      listener = new ReadyListener(panamaHitekArduino);
      this.dataRate=dataRate;
      this.portName=portName;
    }


    public void Connect() throws ArduinoException {

            panamaHitekArduino.arduinoRXTX(portName, dataRate, listener);

    }

    public SerialPortEventListener getDefaultListener() {
        return listener;
    }
}