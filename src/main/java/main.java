import com.panamahitek.ArduinoException;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;

import java.util.logging.Level;
import java.util.logging.Logger;
import com.panamahitek.ArduinoException;
import com.panamahitek.PanamaHitek_Arduino;

public class main {


    static ArduinoConnection arduinoUno = new ArduinoConnection("/dev/ttyACM0",9600);

    public static void main(String[] args) {
        try {
            arduinoUno.Connect();

        } catch (ArduinoException ex) {
            Logger.getLogger(ArduinoConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
