#include <Servo.h>
Servo servo;
String inputString = "";         // a String to hold incoming data
bool stringComplete = false;  // whether the string is complete
String str=""; // auxiliar string
int currentAngle;
int nextAngle;
void setup() {
            Serial.begin(9600);
            while(!Serial)
                Serial.print("-------------------------");
                Serial.print('\n');
                delay(100);
                Serial.print("Arduino is loading....");
                Serial.print('\n');
                delay(100);
                Serial.print("Arduino loaded succesfully");
                Serial.print('\n');
                delay(100);
                Serial.print("-------------------------");
                Serial.print('\n');
                delay(100);
                servo.attach(9);
                
                Serial.print("calibrating servo...");
                Serial.print('\n');
                delay(10);
              servo.write(0);
              delay(1000);
              servo.write(90);
              delay(1000);
              servo.write(180);
              delay(1000);
              servo.write(0);
              delay(1000);
          Serial.print("servo calibrated");
          Serial.print('\n');
          delay(100);
          Serial.flush();
          currentAngle = servo.read();
          Serial.print("Estoy listo\n");
}


void loop() {

    // print the string when a newline arrives:
  if (stringComplete) {
    Serial.flush();
    str= "Setting angle --> "+inputString+" degrees";
    nextAngle = inputString.toInt();
    Serial.print(str+"\n");
    delay(100);
    servo.write(inputString.toInt());
    
    delay(1000);//wait servo to finish setting angle
      Serial.print("Estoy listo\n");
      delay(100);
    
    // clear the string:
    inputString ="";
    stringComplete = false;
  }
    if(  ( servo.read() % 90 ==0 && servo.read()!=currentAngle) ){ //0,90 o 180 y distinto del current.
    currentAngle=servo.read();
    str="current angle changed, now it is:"+String(currentAngle);
    Serial.print(str+"\n");
    delay(100);
   }   
  
  
}
void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:

    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }else{
          inputString += inChar;
    }
  }
}
/*int n = 0; //contador
void loop() {
  Serial.println("1er msj\n");

  String output = "mensaje :";
  //En caso que haya información en el Serial Port, se entra en esta estructura
  while (Serial.available() > 0) {
    //Se lee la información entrante en el Serial Port
    
   
      char input = Serial.read();
     
      if(input!='\n')
      output += input;
      //Se imprime el mensaje en el puerto serie cuando se detecta  fin de linea
      if(input=='\n'){
      Serial.println(output);
      output="mensaje :";
      Serial.flush();
      }
      
      delay(500);
    
  }
}*/
